import { render, screen } from '@testing-library/react';
import React from 'react';
import App from './App';
import Admin from './Admin/Admin';
import OpenTickets from './OpenTickets/OpenTickets';
import ClosedTickets from './ClosedTickets/ClosedTickets';
import TicketBooking from './TicketBooking/TicketBooking';
import Home from './Home/Home';
import Login from './Login/Login';
import Users from './Users/Users';

import renderer from 'react-test-renderer';
import { BrowserRouter as Router } from 'react-router-dom';

test('snapshot test - App', () => {
const tree=renderer.create(<Router><App/></Router>).toJSON();
expect(tree).toMatchSnapshot();
});




test('snapshot test - Admin', () => {
  const tree=renderer.create(<Admin/>).toJSON();
  expect(tree).toMatchSnapshot();
  });


  test('snapshot test - OpenTickets', () => {
    const tree=renderer.create(<OpenTickets/>).toJSON();
    expect(tree).toMatchSnapshot();
    });


    test('snapshot test - ClosedTickets', () => {
      const tree=renderer.create(<ClosedTickets/>).toJSON();
      expect(tree).toMatchSnapshot();
      });


      test('snapshot test - Home', () => {
        const tree=renderer.create(<Home/>).toJSON();
        expect(tree).toMatchSnapshot();
        });


        test('snapshot test - Login', () => {
          const tree=renderer.create(<Login/>).toJSON();
          expect(tree).toMatchSnapshot();
          });



          test('snapshot test - Users', () => {
            const tree=renderer.create(<Users/>).toJSON();
            expect(tree).toMatchSnapshot();
            });


            test('snapshot test - TicketBooking', () => {
              const tree=renderer.create(<TicketBooking/>).toJSON();
              expect(tree).toMatchSnapshot();
              });
  
  